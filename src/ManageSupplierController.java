//import java.net.URL;
//import java.sql.ResultSet;
//import java.sql.SQLException;
//import java.util.Optional;
//import java.util.ResourceBundle;
//import java.util.logging.Level;
//import java.util.logging.Logger;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.fxml.FXML;
//import javafx.fxml.Initializable;
//import javafx.scene.control.Alert;
//import javafx.scene.control.Button;
//import javafx.scene.control.ButtonType;
//import javafx.scene.control.ScrollPane;
//import javafx.scene.control.TableCell;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.scene.control.cell.PropertyValueFactory;
//import javafx.scene.image.Image;
//import javafx.scene.image.ImageView;
//import javafx.scene.layout.HBox;
//import javafx.util.Callback;
//
//
//public class ManageSupplierController implements Initializable {
//
//    @FXML
//private TableView<Supplier> table;
//@FXML
//private TableColumn<Supplier,Integer> id;
//
//@FXML
//private TableColumn<Supplier,String> name;
//
//@FXML
//private TableColumn<Supplier,String> address;
//    
//@FXML
//private TableColumn<Supplier,String> action;  
//
//
//  @FXML
//    private ScrollPane scrollPane;
//
//    @FXML
//    private TextField fname;
//
//    @FXML
//    private TextField fname1;
//
//    @FXML
//    private TextField phonenumber;
//
//    @FXML
//    private TextField emailid;
//
//    @FXML
//    private TextField town;
//
//    @FXML
//    private TextField blockno;
//
//    @FXML
//    private TextField streetno;
//
//    @FXML
//    private TextField city;
//
//    @FXML
//    private TextField pincode;
//
//    @FXML
//    private TextField state;
//
//    @FXML
//    private TextField country;
//
//    @FXML
//    private Button submit;
//
//    @FXML
//    private TextField hiddenTextfield;
//
//    @FXML
//    private TextField companyname;
//    @Override
//    public void initialize(URL url, ResourceBundle rb) {
//        showSuppliers();
//    }    
//     public ObservableList<Supplier> getSupplierList(){
//        ObservableList<Supplier> suppliers = FXCollections.observableArrayList();
//         String sql = "SELECT s.id,CONCAT(s.first_name, \" \", s.last_name) as full_name, concat(a.block_no,\" \",a.street,\" \",a.city,\" \",a.state,\" \",a.country,\" \",a.town,\" \",a.pincode) as full_address\n" +
//"FROM suppliers as s\n" +
//"INNER JOIN\n" +
//"address_supplier as ads\n" +
//"ON s.id = ads.supplier_id\n" +
//"INNER JOIN \n" +
//"address a \n" +
//"ON ads.address_id = a.id AND s.deleted = 0 AND a.deleted = 0"; 
//         ResultSet rs = DbConnect.execute(sql);
//            try {
//                while(rs.next()){
//                suppliers.add(new Supplier(rs.getString("full_name"),rs.getString("full_address"),rs.getString("gst_no")rs.getString("phone_no"),rs.getInt("id"),rs.getInt("phone_no"),rs.getInt("id")));
//                } 
//            } catch (SQLException ex) {   
//                Logger.getLogger(ManageSupplierController.class.getName()).log(Level.SEVERE, null, ex);
//    }   
//    return suppliers;    
// }
//    
//   
//     
// public void showSuppliers(){
//    
//    ObservableList<Supplier> list = getSupplierList();
//    
//    id.setCellValueFactory(new PropertyValueFactory<>("id"));
//    name.setCellValueFactory(new PropertyValueFactory<>("supplierName"));
//    address.setCellValueFactory(new PropertyValueFactory<>("address"));
//
//
////Addong buttons in each cell
//Callback<TableColumn<Supplier,String>,TableCell<Supplier,String>> cellFactory = (param)->{
//    
//    final TableCell<Supplier,String> cell = new TableCell<Supplier,String>(){
//        
//        public void updateItem(String item, boolean empty) {
//            super.updateItem(item, empty);
//            if(empty) {
//                setGraphic(null);
//                setText(null);
//            }
//            else {
//                Image img = new Image("trash.png");
//                ImageView view = new ImageView(img);
//                Image img1 = new Image("pen.png");
//                ImageView view1 = new ImageView(img1);
//                final Button editButton = new Button();
//                final Button deleteButton = new Button();
//                final HBox pane = new HBox(deleteButton, editButton);
//                
//                deleteButton.setStyle("-fx-background-color: transparent;");
//                deleteButton.setStyle("-fx-background-color: transparent;");
////                deleteButton.setPadding(new Insets(10, 5, 10, 0));
//                
//                deleteButton.setGraphic(view);
//                editButton.setGraphic(view1);
//                deleteButton.setOnAction(event -> { 
//                Supplier s = getTableView().getItems().get(getIndex());
//                Alert alert = new Alert(Alert.AlertType.WARNING);
//                alert.setContentText("Are you sure you want to delete " + " " + s.getName() + " " + "record ? ");
//                 String sql= "UPDATE supplier SET deleted = 1 WHERE id = "+ s.getId();
//                 Optional<ButtonType> result = alert.showAndWait();
//                  if(!result.isPresent()) {
//                     // alert is exited, no button has been pressed.
//                  }
//    
//                    else if(result.get() == ButtonType.OK)
//                    {
//                        //oke button is pressed
//                        DbConnect.delete(sql);
//                        showSuppliers();
//                    }
//     
//                    else if(result.get() == ButtonType.CANCEL) {
//                        // cancel button is pressed
//                    }
//                });
//                editButton.setOnAction(event -> { 
//                 
//                        Supplier s = getTableView().getItems().get(getIndex());
//                        scrollPane.setVisible(true);
//                        String employeeName = s.getName();
//                        String[] words = employeeName.split("\\s");
//                            fname.setText(words[0]);
//                            fname1.setText(words[1]);
//                        emailid.setText(s.getEmail());
//                        
//                        hiddenTextfield.setText(s.getId()+"");
//                        phonenumber.setText(s.getPhoneNo()); 
//                        String address = s.getAddress();
//                        String addresswords[] = address.split(";");
//                        blockno.setText(addresswords[0]);
//                        streetno.setText(addresswords[1]);
//                        city.setText(addresswords[2]);
//                        pincode.setText(addresswords[3]);
//                        state.setText(addresswords[4]);
//                        country.setText(addresswords[5]);
//                        town.setText(addresswords[6]);  
//                });
//               
//                setGraphic(deleteButton);
//                setText(null);
//            }
//        }
//    };
// return cell;
//};
//action.setCellFactory(cellFactory);
//table.setItems(list);
//    }    
//    
//}
