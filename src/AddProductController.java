

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;


public class AddProductController implements Initializable {
       @FXML
    private ComboBox<String> category;

    @FXML
    private ComboBox<String> suppliers;

    @FXML
    private Button submit;

    @FXML
    private ComboBox<String> hsncode;
    
    
    @FXML
    private TextField productname;

    @FXML
    private TextField specifications;
    
    @FXML
    private TextField eoq;

    @FXML
    private TextField dangerlevel;

    @FXML
    private TextField quantity;

    @FXML
    private TextField sellingrate;
    
    private String selectedCategory = " ";
    
    
    
    ValidationSupport validationsupport = new ValidationSupport();
    
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Helper helper = new Helper();
        category.setValue("Choose Category:");
        suppliers.setValue("Choose Suppliers:");
        hsncode.setValue("Choose HSN Code:");
        
//        HashMap<String, String> categoryList = new HashMap<>();
//        categoryList = DbConnect.getCategory();
//        helper.setDataInCombobox(category, categoryList);

        HashMap<String, String> categoryList = new HashMap<>();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
        
        HashMap<String, String> supplierList = new HashMap<>();
        supplierList = DbConnect.getSuppliers();
        helper.setDataInComboboxAsHashMap(suppliers, supplierList);
        
        HashMap<String, String> HSNCodeList = new HashMap<>();
        HSNCodeList = DbConnect.getHSNCode();
        helper.setDataInComboboxAsHashMap(hsncode, HSNCodeList);
        
         
        validationsupport.registerValidator(productname, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(sellingrate, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(quantity, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(dangerlevel, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(eoq, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(specifications, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(suppliers, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(category, Validator.createEmptyValidator("Text is required"));
        validationsupport.registerValidator(hsncode, Validator.createEmptyValidator("Text is required"));
    
    
    }    
    
    public void display(){
      submit.setOnAction(e->{
          
              Connection conn = DbConnect.getConnection();
              
              
              selectedCategory = category.getSelectionModel().getSelectedItem();
              String[] words=selectedCategory.split("\\s");
              int categoryId = Integer.parseInt(words[0]);
              String  hsnCode = hsncode.getSelectionModel().getSelectedItem();
              String[] selectedHsn=hsnCode.split("\\s");
              int HSNCode = Integer.parseInt(selectedHsn[1]);
              
              
              String sql = "INSERT into products (name, specification, hsn_code, category_id, eoq_level, "
                      + "danger_level, quantity)" + "values(?,?,?,?,?,?,?)";
              
           
          try {
              PreparedStatement ps = conn.prepareStatement(sql);
//              PreparedStatement ps1 = conn.prepareStatement(sql1);
              ps.setString(1,productname.getText());
              ps.setString(2,specifications.getText());
              ps.setInt(3,HSNCode);
              ps.setInt(4,categoryId);
              ps.setInt(5,Integer.parseInt(eoq.getText()));
              ps.setInt(6,Integer.parseInt(dangerlevel.getText()));
              ps.setInt(7,Integer.parseInt(quantity.getText()));
              ps.execute();
;
          } catch (SQLException ex) {
              Logger.getLogger(AddProductController.class.getName()).log(Level.SEVERE, null, ex);
          }
           
              
              
         
//          try {
//           FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("dashboard.fxml"));
//           Parent  root1 = (Parent) fxmlLoader.load();
//           Stage stage = new Stage();
//           stage.setScene(new Scene(root1));  
//           stage.show();
//          } catch (IOException ex) {
//              Logger.getLogger(AddEmployeeController.class.getName()).log(Level.SEVERE, null, ex);
//          }
      });

    }
}

    



