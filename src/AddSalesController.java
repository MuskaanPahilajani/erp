
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;


public class AddSalesController implements Initializable {
    
    @FXML
    private ComboBox<String> category;

    @FXML
    private ComboBox<String> products;
    
     @FXML
    private TextField finaltotal;
    
    @FXML
    private TextField finalrate;
    
    @FXML
    private TextField discount = null;
    
    @FXML
    private TextField quantity;
    
    private String selectedCategory;
    
    private String  selectedProduct;
      @FXML
    private TextField sellingrate;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        Helper helper = new Helper();
        helper.addingStartingItemInCombobox(category, products);
        HashMap categoryList = new HashMap();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
    }
    
    public void stateChanged() {
        Helper helper = new Helper();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
        helper.setDataInComboboxAsHashMap(products, prod);
        
    }
    
    public void stateChangedOfProduct(){
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
         String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        int sellingRate = DbConnect.getSellingRate(s,s1);
        sellingrate.setText(sellingRate+"");
    }
    
    public void getQuantity() {
         if(!"".equals(quantity.getText())){
        int totalQuantity = Integer.parseInt(quantity.getText());
        int getSellingRate = Integer.parseInt(sellingrate.getText());
        int total = getSellingRate * totalQuantity;
        finalrate.setText(total+"");
        
        if(!"".equals(discount.getText())) {
        int getDiscountRate = Integer.parseInt(discount.getText());
        double discountedRate  = total * getDiscountRate/100;
        total -= discountedRate;
        finalrate.setText(total+"");
        finaltotal.setText(total+"");
        }
    }
    }

}