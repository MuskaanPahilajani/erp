/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;


public class AddPurchaseController implements Initializable {
    
    @FXML
    private ComboBox<String> products;

    @FXML
    private ComboBox<String> category;

    @FXML
    private TextField purchaserate;

    @FXML
    private TextField quantity;

    @FXML
    private ComboBox<String> suppliers;
    
    private String selectedCategory;
    
    private String  selectedProduct;
    
    
    private String selectedSupplier;
    
    



    
    


    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       category.setValue("Choose Category:");
        suppliers.setValue("Choose Suppliers:");
        products.setValue("Choose Products:");
        Helper helper = new Helper();
        HashMap categoryList = new HashMap();
        categoryList = DbConnect.getCategory();
        helper.setDataInComboboxAsHashMap(category, categoryList);
    }
    
    public void stateChangedAll() {
        
        Helper helper = new Helper();
        HashMap<String, String> supplierList = new HashMap<>();
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        
        
        supplierList = DbConnect.getSuppliersAccordingToCategory(s, s1);
        System.out.println(supplierList);
        helper.setDataInComboboxAsHashMap(suppliers, supplierList);
        
        
    
    }
    
   public void stateChanged() {
        Helper helper = new Helper();
        selectedCategory = category.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
        HashMap prod = DbConnect.getProductsAccorgingToCategory(s);
        helper.setDataInComboboxAsHashMap(products, prod);
        
    }
    
    public void stateChangedOfSupplier(){
        
        selectedCategory =  category.getSelectionModel().getSelectedItem();
        selectedProduct = products.getSelectionModel().getSelectedItem();
        String[] selectedCat= selectedCategory.split("\\s");
        String s = selectedCat[0];
         String[] selectedProd= selectedProduct.split("\\s");
        String s1 = selectedProd[0];
        System.out.println(s+" " +s1);
        int purchaseRate = DbConnect.getPurchaseRate(s1,s);
        purchaserate.setText(purchaseRate+"");
    }
    
    
}





